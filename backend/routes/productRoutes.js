const express = require ('express');
const router = express.Router();


//import user controller
const productControllers = require("../controllers/productControllers");

const auth = require("../auth")

const {verify, verifyAdmin} = auth;


//ADD Product
router.post("/addProduct" ,verify, verifyAdmin, productControllers.addProduct);

//archive a Product (ADMIN)
router.put("/archive/:id", verify, verifyAdmin, productControllers.archiveProduct);

//active a Product (ADMIN)
router.put("/activate/:id", verify, verifyAdmin, productControllers.activateProduct);

//delete a Product (ADMIN)
router.delete("/delete/:id", verify, verifyAdmin, productControllers.deleteProduct);

//Get all Products (ADMIN)
router.get("/getAllProduct", verify, verifyAdmin, productControllers.getAllProduct);


//Update a Product (ADMIN)
router.put("/updateProduct/:id", verify, verifyAdmin, productControllers.updateProduct);

router.get("/getAllActiveProducts", productControllers.getAllActiveProducts);

router.get("/:id", productControllers.getSpecificProduct);


module.exports = router;    