const express = require ('express');
const router = express.Router();

//import user controller
const userControllers = require("../controllers/userControllers");

const auth = require("../auth")

const {verify, verifyAdmin} = auth;

                        /*FOR ALL USERS */
//User Registration
router.post('/registration', (req, res) => {
	userControllers.registerUser(req, res).then(resultFromController => res.send(resultFromController))
});

//Login route
router.post('/loginUser', (req, res) => {
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/checkEmail", (req, res) => {
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});


//Change Password
router.put("/changePassword", verify, userControllers.changePassword);

//Get users details
router.get("/getUserDetails", verify, userControllers.getUserDetails);


                    /*FOR REGULAR USER */
//Update User Details
router.put("/updateUserDetails", verify, userControllers.updateUserDetails);




                /*REGULAR USER ORDER PART */
//purchase a Product
router.post("/purchaseProduct" ,verify, userControllers.purchaseProduct);

//Get all users details
router.get("/getUserOrders", verify, userControllers.getUserOrders);

//delete an Order / Cancel Order
router.delete("/cancelOrder/:id", verify, userControllers.cancelOrder)

module.exports = router;