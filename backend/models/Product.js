const mongoose = require ('mongoose');

let productSchema = new mongoose.Schema({

    name : {
        type : String,
        required : [true, "Product Name is Required"]
    },

    description : {
        type : String,
        required : [true, "Product Brand is Required"]
    },

    price : {
        type : Number,
        required : [true, "Product price is Required"]
    },

    stock: {
        type : Number,
        required : [true, "Product Stocks is Required"]
    },

    category : {
        type : String,
        required : [true, "Product Category is Required"]
    },

    isActive : {
        type: Boolean,
        default : true
    },

});

module.exports = mongoose.model("Product",productSchema);