const mongoose = require ('mongoose');

let orderSchema = new mongoose.Schema({

    userId : {
        type : String,
        required : [true, "User ID is Required"]
    },

    productId : {
        type : String,
        required : [true, "Product ID is Required"]
    },
    productName :{
        type : String,
        required : [true, "Product ID is Required"]
    },

    productDescription :{
        type : String,
        required : [true, "Product ID is Required"]
    },

    productPrice :{
        type : Number,
        required : [true, "Product ID is Required"]
    },

    DatePurchased : {
        type : Date,
        default : new Date()
    },
    isActive : {
        type : Boolean,
        default: true
    }

});

module.exports = mongoose.model("Order",orderSchema);