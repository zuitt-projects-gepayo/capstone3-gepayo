const mongoose = require ('mongoose');

let userSchema = new mongoose.Schema({

    firstName : {
        type : String,
        requred: [true, "First Name is required"]
    },

    lastName :  {
        type: String,
        requred: [true, "Last Name is required"]
    },

    address : {
        type: String,
        requred: [true, "Address is required is required"]
    },

    email : {
        type : String,
        requred: [true, "Email is required"]
    },

    password : {
        type : String,
        required : [true, "Password is required"]
    },

    mobileNo : {
        type : String,
        required : [true, "Mobile Number is required"]
    },

    isAdmin : {
        type : Boolean,
        default : false
    },

    isActive : {
        type : Boolean,
        default : true
    },
})

module.exports = mongoose.model("User",userSchema);