const Product = require("../models/Product");













//ADMIN

//add a Product (ADMIN)
module.exports.addProduct = (req, res) => {

    console.log(req.body);

    Product.findOne({name: req.body.name})
    .then(result => {
        if(result !== null && result.name === req.body.name)
        {
            return res.send('Plane Already Added. Please register Another Product')
        }
        else
        {
            let newProduct = new Product ({
                name : req.body.name,
                description : req.body.description,
                price: req.body.price,
                stock: req.body.stock,
                category: req.body.category
            })
                newProduct.save()
                .then(result => res.send(true))
                .catch(err => res.send(err));
        }
    })
    .catch(error => res.send(error))
};


//View all Products (ADMIN)
module.exports.getAllProduct = (req,res) =>{

    Product.find({})
    .then(product => res.send(product))
    .catch(err => res.send(err));
}


//Archive a Product (ADMIN)
module.exports.archiveProduct = (req,res) => {

    console.log(req.params.id);

    let updates = {
        isActive : false
    }

    Product.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then(updatedProduct => res.send(true))
    .catch(err => res.send(err));
};

//Activate a PRODUCT (ADMIN)
module.exports.activateProduct = (req,res) => {

    console.log(req.params.id);

    let updates = {
        isActive : true
    }

    Product.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then(updatedProduct => res.send(true))
    .catch(err => res.send(err));
};

//delete a product  (ADMIN)
module.exports.deleteProduct = (req, res) => {

    console.log(req.params.id) 

     Product.findByIdAndDelete(req.params.id)
     .then(deletedProduct => res.send(true))
    .catch(err => res.send(err));
};

//update a Product (ADMIN)
module.exports.updateProduct =(req, res) => {

    console.log(req.params.id);

    let updateProduct = {

        name : req.body.name,
        description : req.body. description,
        price: req.body. price,
        stock: req.body.stock,
        category: req.body.category
    }

    Product.findByIdAndUpdate(req.params.id, updateProduct, {new: true})
    .then(updatedProduct => res.send(true))
    .catch(err => res.send(err));
};

//REGULAR USER

//view all Active Products
module.exports.getAllActiveProducts = (req, res) =>{

    Product.find({isActive : true}, {"isActive":0, "category":0 })
    .then(product => res.send(product))
    .catch(err => res.send(err));
}

module.exports.getSpecificProduct = (req,res) => {

    Product.findById(req.params.id)
    .then(result => res.send(result))
    .catch(err => res.send(err))

};
