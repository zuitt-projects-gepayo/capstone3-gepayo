const bcrypt = require ('bcrypt');
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const auth = require ("../auth");
const { find, findById } = require('../models/User');


//user Registration
module.exports.registerUser = (req, res) => {

    const hashedPW = bcrypt.hashSync(req.body.password, 10)

    let newUser = new User ({
        firstName : req.body.firstName,
        lastName : req.body. lastName,
        age: req.body. age,
        address : req.body.address,
        email : req.body.email,
        mobileNo : req.body.mobileNo,
        password : hashedPW
    })
    return newUser.save().then((user, err) => {
        if(err){

            return false

        } else {

            return true
        }
    })
};

//Change Password
module.exports.changePassword = (req, res) => {

    console.log (req.user.id);

    const hashedPW = bcrypt.hashSync(req.body.password, 10)

    let updatePassword = {
        password: hashedPW
    }
    User.findByIdAndUpdate(req.user.id,updatePassword,{new: true})
    .then(updatedPassword => res.send(updatedPassword))
    .catch(err => res.send(err));
};

//Login User
module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null){

			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password) 

			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result)}
			} else {

				return false
			}
		}
	})
}

//Check Email exist
module.exports.checkEmailExists = (reqBody) => {
	
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){

			return true

		} else {
			return false
		}
	})
}



//FOR ADMIN








//FOR REGULAR USERS

//Update User Details REGULAR USERS
module.exports.updateUserDetails = (req, res) => {

    console.log(req.user.id);

    let updateUser = {

        firstName : req.body.firstName,
        lastName : req.body. lastName,
        mobileNo : req.body.mobileNo,
        address: req.body.address,
        email : req.body.email
    }

    User.findByIdAndUpdate(req.user.id, updateUser, {new: true})
    .then(updatedUser => res.send(updatedUser))
    .catch(err => res.send(err));
};


//Get User Details
module.exports.getUserDetails = (req,res) => {

	User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(err => res.send(err))

};


//ORDER PART

//purchase a product
module.exports.purchaseProduct = async (req, res) => {

    console.log(req.body);

    if(req.user.isAdmin === true){
        res.send(true)
    }
    else {

        let newOrder = new Order ({

            userId : await req.user.id,
            productId : req.body.productId,
            productName : req.body.productName,
            productPrice : req.body.productPrice,
            productDescription :req.body.productDescription

        })

        Product.findById(req.body.productId)
        .then(result => {
            
            if(result.stock === 0)
            {
                res.send(result)
            }
            else {
                console.log(result.stock)
                
                let productStock = JSON.stringify(result.stock) -1 ;

                let updatestock = {

                   stock : productStock
                }

                console.log(updatestock)
                Product.findByIdAndUpdate(req.body.productId, updatestock, {new: true})
                .then(result =>{
                    newOrder.save()
                    res.send(result)
                })
                .catch(err => res.send(err));
             
            }
           
        })
       
        
    }
};


//View Order Details
module.exports.getUserOrders = (req, res) => {
    console.log(req.user.id)
    Order.find({userId: req.user.id})
    .then(result =>{
        if(result.length ===0){

            res.send(false)
        }
        else
        {
            res.send(result)
        }
    })
    .catch(err => res.send(err))
};

//Cancel an Order
module.exports.cancelOrder = (req, res) => {

    console.log(req.params.id) 

     Order.findByIdAndDelete(req.params.id)
     .then(deletedProduct => res.send(true))
    .catch(err => res.send(err));
};
