const express = require('express'); // for the server
const mongoose = require('mongoose'); // for the database


// allows our backend application to be available in our front-end application
//cors - cross origin resource sharing
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');


const port = process.env.PORT || 4000;
const app = express();

mongoose.connect("mongodb+srv://admin_gepayo:admin169@gepayo-169.td1lc.mongodb.net/capstone3-backend?retryWrites=true&w=majority", 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection Error'));
db.once('open', () => console.log('Connected to MongoDB'));


//middlewares
app.use(express.json())
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.use('/users', userRoutes);
app.use('/products', productRoutes);


app.listen(port, () => console.log(`Server is running at port ${port}`))