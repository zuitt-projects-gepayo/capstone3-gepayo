import { Row,Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function Error (){

    return(
        <section>
        <Row>
            <Col sm={12} md="4" className="text-white mx-auto text-center mt-5">
                <h1>Error 404</h1>
                <p>Go back to </p><Link to="/"><Button variant="warning">Hompage</Button></Link>
            </Col>
        </Row>
        </section>
    )
};