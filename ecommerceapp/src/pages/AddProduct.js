import { Button, Col, Container, Form, Row,  } from "react-bootstrap"
import UserContext from "../UserContext";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { useContext, useState } from "react";


export default function AddProduct() {

    const history = useNavigate();

    const {user} = useContext(UserContext)

    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [stock, setStock] = useState('')
    const [category, setCategory] = useState('')


    const addProduct = (e) => {

        e.preventDefault ();

    

        fetch('http://localhost:4000/products/addProduct',{
            method : "POST",
            headers : {
                'Content-Type' : 'application/json',
                Authorization : `Bearer ${localStorage.getItem("token")}`
            },
            body : JSON.stringify({

                name : name,
                description : description,
                price : price,
                stock : stock,
                category : category,
            })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if(data === true)
                {
                    setName('');
                    setDescription('');
                    setPrice('');
                    setStock('');
                    setCategory('');

                    console.log("Product Added")

                    Swal.fire({
                        title: 'Added a product successful',
                        icon: 'success',
                        text: 'You have added a product!'
                    })

                    history("/adminProduct")

                } else {

                    Swal.fire({
                        title: 'Something went wrong',
                        icon: 'error',
                        text: 'Please try again'
                    })
                }

            })
            
        //clear input fields
        setName('');
        setDescription('');
        setPrice('');
        setStock('');
        setCategory('');
    }
































    return (
        <section>
               <Container>
                   <Row className="py-5">
                       <h1 className="text-center text-white">Add Product</h1>
                       <Col className="mx-auto p-5 formContainer">
                       <Form onSubmit={e => addProduct(e)}>

                        <Form.Group controlId="productName">
                        <Form.Label>Product Name</Form.Label>
                        <Form.Control
                            type="text"
                            value={name}
                            placeholder="Enter Name of A Product"
                            onChange={ e => setName(e.target.value)}
                            required
                        />
                        </Form.Group>


                        <Form.Group controlId="category">
                        <Form.Label>Category</Form.Label>
                        <Form.Select 
                        value={category}
                        onChange={ e => setCategory(e.target.value)}
                        required>
                        <option>Case/Chasis</option>
                        <option>Ram</option>
                        <option>Processor</option>
                        <option>Power Supply</option>
                        <option>CPU Cooler / HeatSink</option>
                        <option>Case fans</option>
                        <option>Motherboard</option>
                        <option>Storage</option>
                        <option>Graphics Card</option>
                        <option>Monitor</option>
                        <option>Chords</option>
                        <option>Others</option>
                        </Form.Select>
                        </Form.Group>

                    
                        <Form.Group controlId="price">
                        <Form.Label>Price</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter Product Price"
                            value={price}
                            onChange={ e => setPrice(e.target.value)}
                            required
                        />
                        </Form.Group>

                        <Form.Group controlId="stock">
                        <Form.Label>Stock</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter number of Stocks"
                            value={stock}
                            onChange={ e => setStock(e.target.value)}
                            required
                        />
                        </Form.Group>

                        <Form.Group controlId="productDescription">
                        <Form.Label>Description</Form.Label>
                        <Form.Control id="descriptionContainer"
                            as="textarea"
                            placeholder="Enter description of Product"
                            value={description}
                            onChange={ e => setDescription(e.target.value)}
                            required
                        />
                        </Form.Group>
                
                <Button variant="warning" type="submit" id="submitBtn" className="mt-3 mb-3" >Submit</Button>
                    
            </Form>
                       </Col>
                   </Row>
               </Container>
            </section>
    )
};