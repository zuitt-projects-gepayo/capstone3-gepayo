import ProductCard from '../components/ProductCard';
import { useEffect, useState } from 'react';
import { Row } from 'react-bootstrap';

export default function Product(){

	// console.log(coursesData);
	// console.log(coursesData[0]);

	const [products, setProducts] = useState();

 	useEffect (() => {
		fetch('http://localhost:4000/products/getAllActiveProducts')
		.then (res => res.json())
		.then (data => {
			setProducts(data.map(product => {
				return(
						<ProductCard key={product._id} productProp={product} />
				);

		}));
		})
	}, [])


	return(
        <section>
            <h1 className='text-white text-center pt-5'>Products</h1>
			<Row className='mx-4'>
				{products}
			</Row>
                
        </section>
			
)
}
