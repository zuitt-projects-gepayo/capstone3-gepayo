import {useState, useEffect} from 'react';
import {Card, Button, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ProductCard({productProp}) {

  
    const { _id, name, description, price, stock} = productProp


    return (
            <Col xs={12} md="4" className='p-1 d-flex justify-content'>
                <Card className='p-4'>
                    <Card.Body  className='cardBody'>
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                        <Card.Subtitle>Stocks:</Card.Subtitle>
                        <Card.Text>{stock}</Card.Text>
                        
                    </Card.Body>
                    <Button variant="warning" as={Link} to ={`/products/${_id}`}>See Details</Button>
                </Card>
            </Col>
    )
}
