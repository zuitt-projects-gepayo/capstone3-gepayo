import { useContext, useState, useEffect } from "react";
import { useNavigate, useParams} from 'react-router-dom'
import { Button, Col, Container, Form, Row,  } from "react-bootstrap"
import UserContext from "../UserContext";
import Swal from "sweetalert2";



export default function UpdateProfile () {

    const {user,setUser} = useContext(UserContext)

    

    const {userId}  = useParams();
    const [email, setEmail] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [address, setAddress] = useState('')
    const [loaded, setLoaded] = useState(() => Date.now())

    //allow us to gain access to methods that will allows us to redirect the user to different page after enrolling a course
    const history = useNavigate();

    const [profile, setProfile] = useState({})

 



    const updateProfile = () => {

        fetch(`http://localhost:4000/users/getUserDetails`,{
            method: "GET",
            headers : {
                 Authorization : `Bearer ${localStorage.getItem("token")}`
            }
        })
            .then (res => res.json())
            .then(data => {
                console.log(data)
    
                setProfile({
                    id : data._id,
                    firstName : data.firstName,
                    lastName : data.lastName,
                    mobileNo : data.mobileNo,
                    address : data.address,
                    email : data.email
                })
               
            })
    }






    useEffect (() => {
        updateProfile();
       
    }, [])


        function updateUser (e) {

            e.preventDefault ();
    
                if(firstName === '' && lastName === ' ' && mobileNo === '' && email === '' &&  address === ''){
                    
                    Swal.fire({
                        title : "Please Fill-out all",
                        icon: "error",
                        text : "Dont leave blank"
                    })
                }
                else {
                    fetch('http://localhost:4000/users/updateUserDetails',{
                        method : "PUT",
                        headers : {
                            Authorization : `Bearer ${localStorage.getItem("token")}`,
                            'Content-Type' : 'application/json'
                       },
                        body : JSON.stringify({
    
                            firstName : firstName,
                            lastName : lastName,
                            mobileNo : mobileNo,
                            address : address,
                        })
                        })
                        .then(res => res.json())
                        .then(data => {
                            console.log(data);

                           
                            setLoaded(Date.now())

                            if(data !== null && data!==undefined && data !== " ")
                            {
                            
                               

                                Swal.fire({
                                    title: 'Update successful',
                                    icon: 'success',
                                    text: 'Welcome!'
                                })
                                
                                history("/profile")
        
                            } else {
        
                                Swal.fire({
                                    title: 'Something went wrong',
                                    icon: 'error',
                                    text: 'Please try again'
                                })
                            }
        
                        })
                }
        }



        return (
            <section key={loaded}>
               <Container>
                   <Row className="py-5">
                       <h1 className="text-center text-white">Update Profile</h1>
                       <Col className="mx-auto p-5 formContainer">
                       <Form onSubmit={e => updateUser(e)}>

                        <Form.Group controlId="firstName">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder= {profile.firstName}
                            value={firstName}
                            onChange={ e => setFirstName(e.target.value)}
                            required
                        />
                        </Form.Group>

                        <Form.Group controlId="lastName">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder= {profile.lastName}
                            value={lastName}
                            onChange={ e => setLastName(e.target.value)}
                            required
                        />
                        </Form.Group>

                        <Form.Group controlId="mobileNo">
                        <Form.Label>Mobile Number</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder= {profile.mobileNo}
                            value={mobileNo}
                            onChange={ e => setMobileNo(e.target.value)}
                            required
                        />
                        </Form.Group>

                        <Form.Group controlId="address">
                        <Form.Label>Address</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder= {profile.address}
                            value={address}
                            onChange={ e => setAddress(e.target.value)}
                            required
                        />
                        </Form.Group>
                
                <Button variant="warning" type="submit" id="submitBtn" className="mt-3 mb-3" >Update</Button>
                    
            </Form>
                       </Col>
                   </Row>
               </Container>
            </section>
        )
}